# TravelAgentMall

TravelAgentMall is an [International B2B flight booking portal](https://www.travelagentmall.com/) in North America, offering Wholesale flight tickets and Large Group Flight Booking for Travel agents, Agency, Travel Management Company, and other Travel Reseller.